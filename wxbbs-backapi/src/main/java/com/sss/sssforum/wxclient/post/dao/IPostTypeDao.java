package com.sss.sssforum.wxclient.post.dao;

import com.sss.sssforum.wxclient.post.entity.PostType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 帖子类型表 Mapper 接口
 * </p>
 *
 * @author sss
 * @since 2020-07-30
 */
public interface IPostTypeDao extends BaseMapper<PostType> {

}
