package com.sss.sssforum.wxclient.sensitiveword.service.impl;

import com.sss.sssforum.wxclient.sensitiveword.entity.SensitiveWord;
import com.sss.sssforum.wxclient.sensitiveword.dao.ISensitiveWordDao;
import com.sss.sssforum.wxclient.sensitiveword.service.ISensitiveWordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 敏感词表 服务实现类
 * </p>
 *
 * @author sss
 * @since 2020-08-16
 */
@Service
public class SensitiveWordServiceImpl extends ServiceImpl<ISensitiveWordDao, SensitiveWord> implements ISensitiveWordService {

}
