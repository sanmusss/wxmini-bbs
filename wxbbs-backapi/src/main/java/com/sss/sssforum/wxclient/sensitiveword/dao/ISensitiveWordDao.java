package com.sss.sssforum.wxclient.sensitiveword.dao;

import com.sss.sssforum.wxclient.sensitiveword.entity.SensitiveWord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 敏感词表 Mapper 接口
 * </p>
 *
 * @author sss
 * @since 2020-08-16
 */
public interface ISensitiveWordDao extends BaseMapper<SensitiveWord> {

}
