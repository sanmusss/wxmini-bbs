package com.sss.sssforum.wxclient.file.service;

import com.sss.sssforum.wxclient.file.entity.FileInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 文件信息表 服务类
 * </p>
 *
 * @author sss
 * @since 2020-08-25
 */
public interface IFileInfoService extends IService<FileInfo> {

}
