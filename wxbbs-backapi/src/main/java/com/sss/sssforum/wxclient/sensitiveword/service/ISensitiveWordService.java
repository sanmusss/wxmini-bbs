package com.sss.sssforum.wxclient.sensitiveword.service;

import com.sss.sssforum.wxclient.sensitiveword.entity.SensitiveWord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 敏感词表 服务类
 * </p>
 *
 * @author sss
 * @since 2020-08-16
 */
public interface ISensitiveWordService extends IService<SensitiveWord> {

}
