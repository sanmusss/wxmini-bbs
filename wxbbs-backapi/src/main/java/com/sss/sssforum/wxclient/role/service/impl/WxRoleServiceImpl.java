package com.sss.sssforum.wxclient.role.service.impl;

import com.sss.sssforum.wxclient.role.entity.WxRole;
import com.sss.sssforum.wxclient.role.dao.IWxRoleDao;
import com.sss.sssforum.wxclient.role.service.IWxRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author sss
 * @since 2020-08-10
 */
@Service
public class WxRoleServiceImpl extends ServiceImpl<IWxRoleDao, WxRole> implements IWxRoleService {

}
