package com.sss.sssforum.wxclient.file.dao;

import com.sss.sssforum.wxclient.file.entity.FileInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 文件信息表 Mapper 接口
 * </p>
 *
 * @author sss
 * @since 2020-08-25
 */
public interface IFileInfoDao extends BaseMapper<FileInfo> {

}
