package com.sss.sssforum.wxclient.role.service;

import com.sss.sssforum.wxclient.role.entity.WxRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author sss
 * @since 2020-08-10
 */
public interface IWxRoleService extends IService<WxRole> {

}
