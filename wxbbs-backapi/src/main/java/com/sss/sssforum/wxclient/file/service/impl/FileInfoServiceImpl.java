package com.sss.sssforum.wxclient.file.service.impl;

import com.sss.sssforum.wxclient.file.entity.FileInfo;
import com.sss.sssforum.wxclient.file.dao.IFileInfoDao;
import com.sss.sssforum.wxclient.file.service.IFileInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 文件信息表 服务实现类
 * </p>
 *
 * @author sss
 * @since 2020-08-25
 */
@Service
public class FileInfoServiceImpl extends ServiceImpl<IFileInfoDao, FileInfo> implements IFileInfoService {

}
