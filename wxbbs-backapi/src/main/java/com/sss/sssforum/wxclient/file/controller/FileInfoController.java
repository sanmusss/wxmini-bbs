package com.sss.sssforum.wxclient.file.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 文件信息表 前端控制器
 * </p>
 *
 * @author sss
 * @since 2020-08-25
 */
@RestController
@RequestMapping("/fileInfo")
public class FileInfoController {

}

