package com.sss.sssforum.wxclient.role.dao;

import com.sss.sssforum.wxclient.role.entity.WxRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author sss
 * @since 2020-08-10
 */
public interface IWxRoleDao extends BaseMapper<WxRole> {

}
