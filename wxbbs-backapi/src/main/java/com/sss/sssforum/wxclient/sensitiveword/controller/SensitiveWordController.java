package com.sss.sssforum.wxclient.sensitiveword.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 敏感词表 前端控制器
 * </p>
 *
 * @author sss
 * @since 2020-08-16
 */
@RestController
@RequestMapping("/sensitiveWord")
public class SensitiveWordController {

}

