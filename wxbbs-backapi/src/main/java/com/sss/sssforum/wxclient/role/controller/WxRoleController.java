package com.sss.sssforum.wxclient.role.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author sss
 * @since 2020-08-10
 */
@RestController
@RequestMapping("/wxRole")
public class WxRoleController {

}

