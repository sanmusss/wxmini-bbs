package com.sss.sssforum.wxclient.post.service;

import com.sss.sssforum.wxclient.post.entity.PostType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 帖子类型表 服务类
 * </p>
 *
 * @author sss
 * @since 2020-07-30
 */
public interface IPostTypeService extends IService<PostType> {

}
