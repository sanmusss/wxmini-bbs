package com.sss.sssforum.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * @author lws
 * @date 2020-08-06 17:20
 **/
@Aspect
@Component
@Slf4j
public class AccessTokenCheck {
}
